from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration, TextSubstitution
from launch_ros.actions import Node

def generate_launch_description():
    jetbot_control_dir = get_package_share_directory('jetbot_stat')
    
    namespace = LaunchConfiguration('namespace')
    config = LaunchConfiguration('config')
    config_path = LaunchConfiguration("config_path")

    use_sim_time = DeclareLaunchArgument(
            'use_sim_time',
            default_value='false',
            description='Use simulation (Gazebo) clock if true'
        )
    log_level = DeclareLaunchArgument(
            "log-level",
            default_value = TextSubstitution(text=str("INFO")),
            description="Logging level"
        )
    
    launchdesc = LaunchDescription([
        use_sim_time,
        log_level,
        DeclareLaunchArgument(
            "namespace",
            default_value='',
        ),
        DeclareLaunchArgument(
            "config",
            default_value='config',
        ),
        DeclareLaunchArgument('config_path', default_value=[
            TextSubstitution(text=f'{jetbot_control_dir}/config/'),
            config, TextSubstitution(text='.yaml')]),
        Node(
            package='jetbot_stat',
            executable='jetbot_stat',
            name='jetbot_stat',
            parameters=[config_path],
            namespace=namespace,
            arguments=['--ros-args', '--log-level', LaunchConfiguration('log-level')],
            output='screen'
        ),
    ])
    
    return launchdesc