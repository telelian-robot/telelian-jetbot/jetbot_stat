import logging
import rclpy
from rclpy.node import Node
from rclpy.parameter import Parameter
from rcl_interfaces.msg import SetParametersResult

from std_msgs.msg import Float32, Int32
from sensor_msgs.msg import BatteryState

import re

from .jetbot_oled.jetbot_oled.jetbot_oled import JetbotOled
from .jetbot_oled.jetbot_oled.network import get_ip_address
from .jetbot_oled.jetbot_oled.stats import get_cpu_usage, get_disk_usage, get_mem_usage
from .jetbot_oled.jetbot_oled.power import StatPower

class JetbotStat(Node):
    def __init__(self):
        super().__init__('jetbot_stat')
        qos_profile = rclpy.qos.QoSProfile(depth=10)
        
        self.declare_parameters(
            namespace='',
            parameters=[
                ('log-level', 'info'),
                ('i2c-bus', 7),
                ('refresh-time', 1),
                ('hwmon-dev', 'hwmon5'),
                ('hwmon-i2c-dev', '7-0041')
            ]
        )
        
        self.__log_level = self.get_parameter('log-level').value
        self.__i2c_bus = self.get_parameter('i2c-bus').value
        self.__refresh_time = self.get_parameter('refresh-time').value
        self.__hwmon_dev = self.get_parameter('hwmon-dev').value
        self.__hwmon_i2c_dev = self.get_parameter('hwmon-i2c-dev').value
        
        self.get_logger().info('init jetbot stat')
        self.get_logger().info(f'{self.__log_level=}')
        self.get_logger().info(f'{self.__i2c_bus=}')
        self.get_logger().info(f'{self.__refresh_time=}')
        self.get_logger().info(f'{self.__hwmon_dev=}')
        self.get_logger().info(f'{self.__hwmon_i2c_dev=}')
        
        self.stat_power = StatPower(i2c_dev_num=self.__hwmon_i2c_dev, hwmon_dev_num=self.__hwmon_dev)
        self.oled = JetbotOled(i2c_bus=self.__i2c_bus)
        
        self.pub_battery = self.create_publisher(BatteryState, 'battery', qos_profile)
        
        self.timer = self.create_timer(self.__refresh_time, self.timer_callback)
        
    def timer_callback(self):
        try:
            ip = get_ip_address('wlan0')
            if ip:
                ip_text = f'wlan0:{ip}'
            else:
                ip = get_ip_address('eth0')
                ip_text = f'eth0:{ip}'
        except Exception as e:
            self.get_logger().error(f'get_ip_address error: {e}')
            ip_text = 'ip:get_ip error'        
        
        battery_msg = BatteryState()
        
        max_voltage = 12.6
        min_voltage = 11.1
        
        powers = self.stat_power.get_stats_int()
        powers = powers.split(' ')
        
        bat = int(powers[0].split(':')[1])
        vcc = int(powers[1].split(':')[1])
        curr = int(powers[2].split(':')[1])
        power = int(powers[3].split(':')[1])
        
        battery_msg.header.stamp = self.get_clock().now().to_msg()
        battery_msg.header.frame_id = 'battery'
        battery_msg.power_supply_technology = BatteryState.POWER_SUPPLY_TECHNOLOGY_LION
        
        
        battery_msg.voltage = bat / 1000
        battery_msg.current = curr / 1000
        battery_msg.percentage = (battery_msg.voltage - min_voltage)/(max_voltage-min_voltage)
        if vcc > 1:
            if battery_msg.voltage>=max_voltage-0.1:
                battery_msg.power_supply_status = BatteryState.POWER_SUPPLY_STATUS_FULL
            else:
                battery_msg.power_supply_status = BatteryState.POWER_SUPPLY_STATUS_CHARGING
        else:
            if battery_msg.current < 0.0:
                battery_msg.power_supply_status = BatteryState.POWER_SUPPLY_STATUS_DISCHARGING
            else:
                battery_msg.power_supply_status = BatteryState.POWER_SUPPLY_STATUS_NOT_CHARGING
            
        self.pub_battery.publish(battery_msg)
        
        self.get_logger().debug(f'{bat=},{curr=} {power=} {vcc=}')
        
        text = [
            ip_text,
            str(get_mem_usage()),
            f'Bt:{bat/1000:.2f}V V:{vcc/1000:.2f}mV',
            f'I:{curr/1000:.2f}A P:{power/1000000:.2f}W',
        ]
        self.oled.render(text)        

        
import sys
import argparse
import logging
def main(args=None):
    rclpy.init(args=args)
    node = JetbotStat()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.get_logger().info('Keyboard Interrupt (SIGINT)')
        node.get_logger().info('jetbot_stat closed')
    finally:
        node.timer.cancel()
        node.oled.clear()
        node.destroy_node()
    
if __name__ == '__main__':
    main()