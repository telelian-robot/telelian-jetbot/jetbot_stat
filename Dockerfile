FROM ros_humble_jetbot_build:latest

ARG PROJECT_NAME=jetbot_stat
RUN mkdir -p /workspaces/src/$PROJECT_NAME
COPY . /workspaces/src/$PROJECT_NAME/
WORKDIR /workspaces/src

SHELL ["/bin/bash", "-c"] 

RUN sudo apt update && sudo apt install -y net-tools \
    && pip install -r jetbot_stat/jetbot_stat/jetbot_oled/requirements.txt \
    && source /opt/ros/humble/setup.bash \
    && cd /workspaces \
    && sudo rosdep install --from-paths src --ignore-src --rosdistro humble -y \
    && colcon build --symlink-install \
    && sudo apt-get autoremove -y \
    && sudo apt-get clean \
    && sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENTRYPOINT [ "bash", "-c", "source install/setup.bash&& \"$@\"", "-s" ] 

