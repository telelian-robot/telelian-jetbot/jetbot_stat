# jetbot stat ros2 docker [ros:humble]


#### build
```bash
docker build -t ros_humble_jetbot_stat . -f Dockerfile
```

#### run
```bash
docker run -it --rm --network host \
--privileged \
-e FASTRTPS_DEFAULT_PROFILES_FILE=/usr/local/share/middleware_profiles/rtps_udp_profile.xml \
-v $HOME/workspaces:/workspaces \
--workdir /workspaces \
--user="admin" \
--name jetbot_stat \
ros_humble_jetbot_stat:latest \
/bin/bash
```